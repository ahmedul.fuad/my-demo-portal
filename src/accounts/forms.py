from django import forms
from django.contrib.auth.models import User 
#this form inherits from register form (user creattion form)
from django.contrib.auth.forms import UserCreationForm

from .models import Profile

class UserRegisterForm(UserCreationForm):
	email = forms.EmailField()

	class Meta:		#class configuration
		model = User   #whenever this form is validates it's going to create a new user
		fields = ['username', 'email', 'password1', 'password2']


class UserUpdateForm(forms.ModelForm):
	email = forms.EmailField()

	class Meta:
		model = User
		fields = ['username', 'email']


class ProfileUpdateForm(forms.ModelForm):
	#no additional field
	class Meta:
		model = Profile
		fields = ['image']
